const Gpio = require('onoff').Gpio; //include onoff to interact with the GPIO
const Parse = require('parse/node');
var LED = new Gpio(12, 'out'); //use GPIO pin 17 as input, and 'both' button presses, and releases should be handled
var pushButton = new Gpio(5, 'in', 'both'); //use GPIO pin 17 as input, and 'both' button presses, and releases should be handled

Parse.serverURL = 'https://parseapi.back4app.com'; // This is your Server URL
Parse.initialize(
    'r7qXwkQ3l5BG9zNjRQCwDp6SpT6w3dY30W32z62q', // This is your Application ID
    'tf4ZQZAWUxbFECgOE333P4mB9athuIRWX2HNfAv7', // This is your Javascript key
    'ZcpA6A4bDU43Cn57pPb3sPRe5VRLq4AYc082mHRd' // This is your Master key (never use it in the frontend)
);

pushButton.watch((err, value) => {
    if(err){
        console.log(err + "hello there")
        return
    }
    console.log(value)
    LED.writeSync(value)
})
function unexportOnClose() { //function to run when exiting program
    LED.writeSync(0); // Turn LED off
    LED.unexport(); // Unexport LED GPIO to free resources
    pushButton.unexport(); // Unexport Button GPIO to free resources
};
process.on('SIGINT', unexportOnClose); //function to run when user closes using ctrl+c

